//
//  APISettings.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 01/11/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation


class APISettings {
    
    
    init() {
        
    }
    
    let login_headers = [
        "Content-Type": "application/x-www-form-urlencoded"
    ]
    var GET_headers = [
        "Accept": "application/json",
        
    ]
    
    let base_URL = "https://api.lufthansa.com"
    let api_version = "v1"
    let grant_type = "client_credentials"
    let language_code = "en"
    let short_name = "Short Name"
    var access_token :String = "" {
        didSet{
            GET_headers["Authorization"] = "Bearer " + access_token
        }
    }
}

struct Arguments {
    static let client_id = "client_id"
    static let client_secret = "client_secret"
    static let grant_type = "grant_type"
}

