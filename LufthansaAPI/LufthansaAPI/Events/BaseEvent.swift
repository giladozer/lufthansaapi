//
//  BaseEvent.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 05/11/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation


class BaseEvent
{
    init () {}
    
    var type:String = ""
    var data:AnyObject = AnyObject()
    
}