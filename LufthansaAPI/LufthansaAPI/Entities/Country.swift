//
//  Country.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 05/11/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation


class Country:BaseEntity,CustomStringConvertible  {
    
    init(countryCode:String, zoneCode:String,name:String){
        self.countryCode = countryCode
        self.zoneCode = zoneCode
        self.name = name
    }
    
    var countryCode:String
    var zoneCode:String
    var name:String
    
    
    var description: String  {
        return name;
    }
    
}