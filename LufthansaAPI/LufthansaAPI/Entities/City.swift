//
//  City.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 07/11/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation


class City:BaseEntity,CustomStringConvertible  {
    
    init(cityCode:String, countryCode:String,name:String){
        self.countryCode = countryCode
        self.cityCode = cityCode
        self.name = name
        self.airportCode = ""
        self.position = Position()
    }
    
    init(cityCode:String, countryCode:String,name:String, airportCode:AnyObject, position:Position){
        self.countryCode = countryCode
        self.cityCode = cityCode
        self.name = name
        self.airportCode = airportCode
        self.position = position
    }
    
    var countryCode:String
    var cityCode:String
    var name:String
    var airportCode:AnyObject // either String for one airport or array of string for mutliple airports
    var position:Position
    
    
    var description: String  {
        return name;
    }
    
}