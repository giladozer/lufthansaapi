//
//  Airline.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 06/12/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation


class Airline: BaseEntity {
    
    init(airlineID:String, airlineID_ICAO:String,name:String, shortName:String){
        self.airlineID = airlineID
        self.airlineID_ICAO = airlineID_ICAO
        self.name = name
        self.shortName = shortName
    }
    
    var airlineID:String
    var airlineID_ICAO:String
    var name:String
    var shortName:String 
    
    
    var description: String  {
        return name;
    }

    
}