//
//  Airport.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 06/12/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation


// it doesn't make much sense but City and Aiport are exactly the same thing only that airportcode will only have 1 item.

class Airport: City {
    
    init(cityCode:String, countryCode:String,name:String, airportCode:AnyObject, position:Position, locationType:String){
        self.locationType = locationType
        super.init(cityCode: cityCode, countryCode: countryCode, name: name, airportCode: airportCode, position: position)
        
    }
    
    var locationType:String

}