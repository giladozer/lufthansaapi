//
//  Position.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 07/11/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation


class Position {
    
    init(latitude:Double = 0.0, longitude:Double = 0.0) {
        self.latitude = latitude
        self.longitude = longitude
    }
    var latitude:Double
    var longitude:Double 
}