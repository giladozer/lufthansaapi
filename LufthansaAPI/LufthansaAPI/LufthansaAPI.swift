//
//  LufthansaAPI.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 04/11/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class LufthansaAPI {
    
    static let sharedInstance = LufthansaAPI()
    
    private init() {
    }
    
    var settings = APISettings()
    
    let referenceData = ReferenceData()
    
    
    func login(client_id:String, client_secret:String) {
        
        //
        //        Alamofire.request(.GET, Settings.base_URL + "/" + Settings.api_version + "/" + "references/airports/FRA", headers:headers)
        //            .responseJSON { response in
        //                print(response.request)  // original URL request
        //                print(response.response) // URL response
        //                print(response.data)     // server data
        //                print(response.result)   // result of response serialization
        //
        //                if let JSON = response.result.value {
        //                    print("JSON: \(JSON)")
        //                }
        //        }
        
        let parameters = [
            Arguments.client_secret: client_secret,
            Arguments.grant_type: settings.grant_type,
            Arguments.client_id: client_id
        ]
        
        Alamofire.request(.POST, settings.base_URL + "/" + settings.api_version + "/" + "oauth/token", parameters: parameters, headers: settings.login_headers).responseJSON { response in
            print(response.request)  // original URL request
            print("request header: \(response.request?.allHTTPHeaderFields)!)")
            print("request body: \(NSString(data:(response.request?.HTTPBody)!, encoding:NSUTF8StringEncoding) as String?)") // original URL BODY
            print(response.response) // URL response
            print(response.data)     // server data
            print(response.result)   // result of response serialization
            if let data = response.result.value {
               // print("JSON: \(data)")
                let gilad = JSON(data)
                self.settings.access_token = gilad["access_token"].string!
                print(self.settings.access_token)
//                self.referenceData.getCountries() { result in
//                    print(result)
//                }
                
//                self.referenceData.getNearestAirports("51.5", longitude:"-0.142") { result in
//                    print(result)
//                }
                
                self.referenceData.getAirlines("LY") { result in
                    print(result)
                }
                
            }
            
        }

    }
    
}
