//
//  BaseMethods.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 04/11/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class BaseMethods {
    
    var directory = ""
    var settings:APISettings {
        get {
            return LufthansaAPI.sharedInstance.settings
        }
    }
    
    func getRequest(suffixURL:String,parameters: [String: AnyObject]? = nil, completionHandler: Response<AnyObject, NSError> -> Void){
        
        Alamofire.request(.GET, settings.base_URL + "/" + settings.api_version + "/" + directory + suffixURL, parameters: parameters, headers:settings.GET_headers)
            .responseJSON(completionHandler: completionHandler)
            //{ response in
//                print(response.request)  // original URL request
//                print(response.response) // URL response
//                print(response.data)     // server data
//                print(response.result)   // result of response serialization
//                
//                if let JSON = response.result.value {
//                    print("JSON: \(JSON)")
//                }
                
                //self.handleResult(response)
 //       }

    }
    
    
//    func handleResult(response: Response<AnyObject, NSError>)->Void {
//        //impl in subclasses
//    }
    
}
