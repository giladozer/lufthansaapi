//
//  ReferenceData.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 04/11/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class ReferenceData : BaseMethods {
    
    override init(){
        super.init()
        
        directory = "references/"
    }
    
    func getCountries(countryCode: String = "",languageCode:String = LufthansaAPI.sharedInstance.settings.language_code,completionHandler: AnyObject-> Void){
        
        getRequest("countries/\(countryCode)",parameters: ["lang":languageCode]){ response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            
            if let result = response.result.value {
                print("JSON: \(result)")
                if let countries:JSON = JSON(result)["CountryResource"]["Countries"]["Country"] {
                    
                    //                    print(countries.count)
                    //                    print(countries.array)
                    //                    print(countries.boolValue)
                    //                    print(countries.string)
                    //                    print(countries.startIndex)
                    //                    print(countries.arrayValue)
                    //                    print(countries.type)
                    
                    if(countries.type == Type.Array) {
                        completionHandler(EntityFactory.createCountries(countries))
                        
                    } else{
                        completionHandler(EntityFactory.createCountry(countries))
                    }
                    
                    
                }
                
            }
            
            
        }
        
        
    }
    
    
    func getCities(cityCode: String = "",languageCode:String = LufthansaAPI.sharedInstance.settings.language_code,completionHandler: AnyObject-> Void){

        
        getRequest("cities/\(cityCode)",parameters: ["lang":languageCode]){ response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            
            if let result = response.result.value {
                print("JSON: \(result)")
                if let cities:JSON = JSON(result)["CityResource"]["Cities"]["City"] {
                    
                    //                    print(countries.count)
                    //                    print(countries.array)
                    //                    print(countries.boolValue)
                    //                    print(countries.string)
                    //                    print(countries.startIndex)
                    //                    print(countries.arrayValue)
                    //                    print(countries.type)
                    
                    if(cities.type == Type.Array) {
                        completionHandler(EntityFactory.createCities(cities))
                        
                    } else{
                        completionHandler(EntityFactory.createCity(cities))
                    }
                    
                    
                }
                
            }
            
            
        }
        
        
    }
    
    func getAirports(airportCode: String = "",languageCode:String = LufthansaAPI.sharedInstance.settings.language_code,completionHandler: AnyObject-> Void){
        
        
        getRequest("airports/\(airportCode)",parameters: ["lang":languageCode]){ response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            
            if let result = response.result.value {
                print("JSON: \(result)")
                if let airports:JSON = JSON(result)["AirportResource"]["Airports"]["Airport"] {
                    
                    //                    print(countries.count)
                    //                    print(countries.array)
                    //                    print(countries.boolValue)
                    //                    print(countries.string)
                    //                    print(countries.startIndex)
                    //                    print(countries.arrayValue)
                    //                    print(countries.type)
                    
                    if(airports.type == Type.Array) {
                        completionHandler(EntityFactory.createAirports(airports))
                        
                    } else{
                        completionHandler(EntityFactory.createAirport(airports))
                    }
                    
                    
                }
                
            }
            
            
        }
        
        
    }
    
    func getNearestAirports(latitude: String,longitude: String,languageCode:String = LufthansaAPI.sharedInstance.settings.language_code,completionHandler: AnyObject-> Void){
        
        
        getRequest("airports/nearest/\(latitude),\(longitude)",parameters: ["lang":languageCode]){ response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            
            if let result = response.result.value {
                print("JSON: \(result)")
                if let airports:JSON = JSON(result)["NearestAirportResource"]["Airports"]["Airport"] {
                    
                    //                    print(countries.count)
                    //                    print(countries.array)
                    //                    print(countries.boolValue)
                    //                    print(countries.string)
                    //                    print(countries.startIndex)
                    //                    print(countries.arrayValue)
                    //                    print(countries.type)
                    
                    if(airports.type == Type.Array) {
                        completionHandler(EntityFactory.createAirports(airports))
                        
                    } else{
                        completionHandler(EntityFactory.createAirport(airports))
                    }
                    
                    
                }
                
            }
            
            
        }
        
        
    }
    
    
    func getAirlines(airlineCode: String = "",languageCode:String = LufthansaAPI.sharedInstance.settings.language_code,completionHandler: AnyObject-> Void){
        
        
        getRequest("airlines/\(airlineCode)",parameters: ["lang":languageCode]){ response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            
            if let result = response.result.value {
                print("JSON: \(result)")
                if let airlines:JSON = JSON(result)["AirlineResource"]["Airlines"]["Airline"] {
                    
                    //                    print(countries.count)
                    //                    print(countries.array)
                    //                    print(countries.boolValue)
                    //                    print(countries.string)
                    //                    print(countries.startIndex)
                    //                    print(countries.arrayValue)
                    //                    print(countries.type)
                    
                    if(airlines.type == Type.Array) {
                        completionHandler(EntityFactory.createAirlines(airlines))
                        
                    } else{
                        completionHandler(EntityFactory.createAirline(airlines))
                    }
                    
                    
                }
                
            }
            
            
        }
        
        
    }



    
    
}
