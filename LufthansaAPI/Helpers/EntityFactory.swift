//
//  EntityFactory.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 05/11/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation
import SwiftyJSON


class EntityFactory {
    
    static func createCountry(country:JSON)->Country{
        
        var countryName:String = ""
        var countryCode:String = ""
        var zoneCode:String = ""
        
        
        if (country["CountryCode"].isExists()){
            countryCode = country["CountryCode"].string!
        }
        
        if(country["ZoneCode"].isExists()){
            zoneCode = country["ZoneCode"].string!
        }
        
        
        if let names:JSON = country["Names"]["Name"] {
            print(names)
            countryName = names["$"].string!
            
//            for (_,name):(String, JSON) in names {
//                print(name)
//                if (name["@LanguageCode"].string == LufthansaAPI.sharedInstance.settings.language_code){
//                    countryName = name["$"].string!
//                    break
//                }
//            }
        }
        
        let result:Country = Country(countryCode: countryCode, zoneCode: zoneCode, name:countryName )
        result.source = country
        
        return result
    }
    
    static func createCountries(countriesList:JSON)->Array<Country>{
        //print("Gilad :::: \(country["CountryResource"]["Countries"]["Country"][0])")
        
        var result = Array<Country>()
        
        for (_,country):(String, JSON) in countriesList {
            let countryInstance = createCountry(country)
            countryInstance.source = country
            result.append(countryInstance)
        }
        
        
        
        //        var name = country["Names"]
        //        print(country["CountryCode"])
        //        print(country["ZoneCode"].string)
        //        print(name)
        
        return result
    }
    
    
    static func createCity(city:JSON)->City{
        
        var cityName:String = ""
        var cityCode:String = ""
        var countryCode:String = ""
        var airportCode:AnyObject = ""
        let position:Position = Position()
        
        
        if (city["CountryCode"].isExists()){
            countryCode = city["CountryCode"].string!
        }
        
        if(city["CityCode"].isExists()){
            cityCode = city["CityCode"].string!
        }
        
        
        if let names:JSON = city["Names"]["Name"] {
            cityName = names["$"].string!
            
            //            for (_,name):(String, JSON) in names {
            //                print(name)
            //                if (name["@LanguageCode"].string == LufthansaAPI.sharedInstance.settings.language_code){
            //                    countryName = name["$"].string!
            //                    break
            //                }
            //            }
        }
        
        if(city["Airports"]["AirportCode"].isExists()){
            if (city["Airports"]["AirportCode"].type == Type.String){
                 airportCode = city["Airports"]["AirportCode"].string!
            }else if (city["Airports"]["AirportCode"].type == Type.Array){
                airportCode = city["Airports"]["AirportCode"].arrayObject!
            }
        }
        
        if(city["Position"]["Coordinate"].isExists()){
           position.latitude = city["Position"]["Coordinate"]["Latitude"].numberValue as Double
            position.longitude = city["Position"]["Coordinate"]["Longitude"].numberValue as Double
        }

        let result:City = City(cityCode: cityCode, countryCode: countryCode, name:cityName,airportCode:airportCode, position:position)
        
        result.source = city
        return result
    }
    
    static func createCities(citiesList:JSON)->Array<City>{
        //print("Gilad :::: \(country["CountryResource"]["Countries"]["Country"][0])")
        
        var result = Array<City>()
        
        for (_,city):(String, JSON) in citiesList {
            let cityInstance = createCity(city)
            cityInstance.source = city
            result.append(cityInstance)
        }
        
        
        
        //        var name = country["Names"]
        //        print(country["CountryCode"])
        //        print(country["ZoneCode"].string)
        //        print(name)
        
        return result
    }
    
    static func createAirport(city:JSON)->Airport{
        
        var cityName:String = ""
        var cityCode:String = ""
        var countryCode:String = ""
        var airportCode:AnyObject = ""
        var locationType:String = ""
        let position:Position = Position()
        
        print("json desc: " + city.description)
        
        
        if (city["CountryCode"].isExists()){
            countryCode = city["CountryCode"].string!
        }
        
        if(city["CityCode"].isExists()){
            cityCode = city["CityCode"].string!
        }
        
        
        if let names:JSON = city["Names"]["Name"] {
            cityName = names["$"].string!
            
            //            for (_,name):(String, JSON) in names {
            //                print(name)
            //                if (name["@LanguageCode"].string == LufthansaAPI.sharedInstance.settings.language_code){
            //                    countryName = name["$"].string!
            //                    break
            //                }
            //            }
        }
        
        if city["AirportCode"].isExists() {
            airportCode = city["AirportCode"].string!
        }
        
        if city["LocationType"].isExists() {
            locationType = city["LocationType"].string!
        }
        
        if(city["Position"]["Coordinate"].isExists()){
            position.latitude = city["Position"]["Coordinate"]["Latitude"].numberValue as Double
            position.longitude = city["Position"]["Coordinate"]["Longitude"].numberValue as Double
        }
        
        let result:Airport = Airport(cityCode: cityCode, countryCode: countryCode, name:cityName,airportCode:airportCode, position:position, locationType: locationType)
        
        result.source = city
        print(result)
        return result
    }
    
    static func createAirports(airportsList:JSON)->Array<Airport>{
        //print("Gilad :::: \(country["CountryResource"]["Countries"]["Country"][0])")
        
        var result = Array<Airport>()
        
        for (_,airport):(String, JSON) in airportsList {
            let airportInstance = createAirport(airport)
            airportInstance.source = airport
            result.append(airportInstance)
        }
        
        
        
        //        var name = country["Names"]
        //        print(country["CountryCode"])
        //        print(country["ZoneCode"].string)
        //        print(name)
        
        return result
    }
    
    
    static func createAirline(airline:JSON)->Airline{
        
        var airlineName:String = ""
        var airlineID:String = ""
        var airlineID_ICAO:String = ""
        var shortName:String = ""
        
        
        if (airline["AirlineID"].isExists() && airline["AirlineID"].type == Type.String){
            airlineID = airline["AirlineID"].string!
        }
        
        if (airline["AirlineID_ICAO"].isExists() && airline["AirlineID_ICAO"].type == Type.String){
            airlineID_ICAO = airline["AirlineID_ICAO"].string!
        }
        
        
        if let names:JSON = airline["Names"]["Name"] {
            airlineName = names["$"].string!
            
            //            for (_,name):(String, JSON) in names {
            //                print(name)
            //                if (name["@LanguageCode"].string == LufthansaAPI.sharedInstance.settings.language_code){
            //                    countryName = name["$"].string!
            //                    break
            //                }
            //            }
        }
        
        if let otherId:JSON = airline["OtherIDs"]["OtherID"]{
            if (otherId["@Description"].string == LufthansaAPI.sharedInstance.settings.short_name){
                shortName = otherId["$"].string!
            }
        }
        
        let result:Airline = Airline(airlineID: airlineID, airlineID_ICAO: airlineID_ICAO, name: airlineName, shortName: shortName)
        
        result.source = airline
        return result
    }
    
    static func createAirlines(airlinesList:JSON)->Array<Airline>{
        
        var result = Array<Airline>()
        
        for (_,airline):(String, JSON) in airlinesList {
            let airlineInstance = createAirline(airline)
            airlineInstance.source = airline
            result.append(airlineInstance)
        }
        
        return result
    }



}