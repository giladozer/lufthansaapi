//
//  APIMenuItem.swift
//  LufthansaAPI
//
//  Created by Gilad Ozer on 08/11/2015.
//  Copyright © 2015 boOola. All rights reserved.
//

import Foundation


class APIMenuItem {
    
    init(name:String) {
        self.name = name
    }
    
    var name:String
    
}


class APIMenuItems {
    static let referenceData:String = "Reference Data"
    static let offers:String = "Offers"
    static let operations:String = "Operations"
}